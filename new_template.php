<div class="container">
    <div class="row">
        <div class="featured col-sm-12">
            <article class="nopadding">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/08/Door-people-exiting-IDEAS-580x405.jpg" class="img-responsive">
                    </div>
                    <div class="col-sm-6">
                        <h1><span class="source small"><a href="#">Management</a></span><br><a href="#">When the boss leaves, should I stay, or should I go ?</a></h1>
                        <p>When good leaders leave for new opportunities, they might walk out the door with more than best wishes and the last slices of farewell cake. Some subordinates might follow the leader to his or her new venture. How can companies prevent one leader’s departure from turning into a full-fledged flight?</p>
                        <a class="btn btn-primary">Read More</a>
                    </div>
                </div>
            </article>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/08/Smart-phone-IDEAS-380x215.jpg" class="img-responsive">
            <h2 class="h4"><span class="source small"><a href="#">Managing IT</a></span><br><a href="#">Are employee devices an unlocked window to your data?</a></h2>
            <p>Think again if you believe that those complex, eight-characters-or-more, upper case, lower case and special character-filled passwords you require do a great job of protecting your corporate IT systems. In reality,…</p>
        </div>
        <div class="col-lg-3">
            <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/07/Ashforth-Blake-2016-IDEAS-380x215.jpg" class="img-responsive">
            
            <h2 class="h4"><span class="source small"><a href="#">Management</a></span><br><a href="#">Why you need to avoid the office bully</a></h2>
            <p>When new employees come on board, the boss wants them to identify with co-workers so they form a strong team. But new research by Professor of Management Blake Ashforth has found that personal identification in the…</p>
        </div>
        <div class="col-lg-3">
            <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/08/Farm-business-IDEAS-380x215.jpg" class="img-responsive">
            <h2 class="h4"><span class="source small"><a href="#">Agribusiness</a></span><br><a href="#">Betting the farm on income diversification</a></h2>
            <p>Small farm businesses find it particularly difficult to remain competitive in the business of raising produce and livestock. Given the income challenges small farms face, it’s not surprising that two thirds of those…</p>
        </div>
        <div class="col-lg-3">
            <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/06/Cowboy-IDEAS-380x215.jpg" class="img-responsive">
            
            <h2 class="h4"><span class="source small"><a href="#">Managing IT</a></span><br><a href="#">Taming the Wild West of embedded analytics</a></h2>
            <p>Information Systems Professors Michael Goul, Raghu Santanam and Robert St. Louis are mapping ways to create standardization across firms, bringing order to predictive analytics -- something that’s not even close to…</p>
        </div>
    </div>
</div>
<div class="background-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 special">
                <div class="row">
                    <div class="col-lg-4">
                        <h2 class="h4"><a href="#">From the Podium</a></h2>
                        <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/02/LifeLock-28_88-ideas-380x215.jpg" class="img-responsive">
                        <h3 class="h5"><a href="#">Identity protection versus identity management</a></h3>
                        <table class="table table-condensed">
                           <tr><td><a href="#">Blue Nile diamonds: Trust and authenticity</a></td></tr>
                           <tr><td><a href="#">Starbucks’ success brewed with a commitment to humanity</a></td></tr>
                           <tr><td><a href="#">Podcast: Everyone wins when more graduate from college</a></td></tr>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <h2 class="h4"><a href="#">Social Media Insights</a></h2>
                        <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2014/09/social-icons-on-ipad_200-380x215.jpg" class="img-responsive">
                        <h3 class="h5"><a href="#">Political polarization: Does social media make it worse?</a></h3>
                        <table class="table table-condensed">
                           <tr><td><a href="#">How social media changes the playing field</a></td></tr>
                           <tr><td><a href="#">Not just impulse: Mobile promotions work longer than expected</a></td></tr>
                           <tr><td><a href="#">Transformative: Social media throughout the firm</a></td></tr>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <h2 class="h4"><a href="#">Consumer Behavior</a></h2>
                        <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2014/12/medicare_part_d-380x215.png" class="img-responsive">
                        <h3 class="h5"><a href="#">More is less? In health insurance markets, more is more</a></h3>
                        <table class="table table-condensed">
                           <tr><td><a href="#">Self-improvement: How much help do consumers want?</a></td></tr>
                           <tr><td><a href="#">Trying to lose weight? Look around the table, not just on it</a></td></tr>
                           <tr><td><a href="#">Eat, drink and go shopping: Why thoughts of death whet consumer appetite for stuff</a></td>
                        </table>
                    </div>
                </div>
            </div>
            <aside class="col-lg-4">
                <div class="widget"><img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/04/spring-magazin-spring.jpg" class="img-responsive"></div>
            </aside>
        </div>
    </div>
</div>
<div class="container">
<div class="section-title text-center h2">W. P. Carey in the News</div>
    <div class="row">
        <div class="col-lg-4">
            <article class="nopadding">
                <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/08/Woma-stressed-glasses-IDEAS-380x215.jpg" class="img-responsive">
                <div class="panel-body">
                
                <h3 class="h4"><span class="source small"><a href="#">Harvard Business Review</a></span><br><a href="#">How anxiety affects CEO decision makint</a></h3>
                <p>In a Harvard Business Review article, Professor of Management Luis Gomez-Mejia and his research team reported on the...</p>
                </div>
            </article>
        </div>
        <div class="col-lg-4">
            <article class="nopadding">
                <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/05/Payday-IDEAS-380x215.jpg" class="img-responsive">
                <div class="panel-body">
                
                <h3 class="h4"><span class="source small"><a href="#">Bloomberg</a></span><br><a href="#">Wages are imperfect window into health of U.S. labor market</a></h3>
                <p>Up until recently the U.S. labor market has been viewed as on the uptick, especially after research conducted by the...</p>
                </div>
            </article>
        </div>
        <div class="col-lg-4">
            <article class="nopadding">
                <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/08/People-job-applicants-IDEAS-380x215.jpg" class="img-responsive">
                <div class="panel-body">
                	
                	<h3 class="h4"><span class="source small"><a href="#">The Arizona Republic</a></span><br><a href="#">Arizona’s jobless rate climbs fourth month in a rot</a></h3>
                	<p>Arizona's unemployment rate rose in July for the fourth consecutive month, which dampens news that the state's</p>
                </div>
            </article>
        </div>
    </div>
</div>