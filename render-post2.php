<?php 
function render_post2($postObjAll) { 

          // postObjAll has the following properties:
            // 1. postTitle
            // 2. postExcerpt
            // 3. postLink
            // 4. postIMGUrl
            // 5. postCategoryName
            // 6. postCategoryURL

    $postObj = $postObjAll[0];
    $sectionTitleObject = $postObjAll[1];  

    $value = (array) $sectionTitleObject;
    $section_titles = $value['section_titles'];
    $section_keywords = $value['section_keywords'];
    $section_tags = $value['section_tags'];
    ?>



    <div class="container">
    <div class="row">
        <div class="featured col-sm-12">
            <article class="nopadding">
                <div class="row">
                    <div class="col-sm-6">
                         <?php echo "<a href='".$postObj[0]->postLink."'><img src='".$postObj[0]->postIMGUrl."' class='img-responsive'/> </a>  "; ?>                       
                    </div>
                    <div class="col-sm-6">
                        <h1><span class="source small"><?php echo"<a href=".$postObj[0]->postCategoryURL.">";?> <?php echo $postObj[0]->postCategoryName;?>
                            </a></span><br><a href="<?php echo $postObj[0]->postLink; ?>"><?php echo $postObj[0]->postTitle ?></a></h1>
                        <p><?php echo $postObj[0]->postExcerpt; ?></p>
                        <a class="btn btn-primary" href="<?php echo $postObj[0]->postLink; ?>">Read More</a>
                    </div>
                </div>
            </article>
        </div>
    </div>
    <div class="row">
    <?php for($i = 1; $i <= 4; $i++) { ?>
        <div class="col-lg-3">
            <?php echo "<a href='".$postObj[$i]->postLink."'><img src='".$postObj[$i]->postIMGUrl."' class='img-responsive'/> </a>  "; ?> 
            <h2 class="h4"><span class="source small"><a href="<?php echo $postObj[$i]->postCategoryURL; ?>"><?php echo $postObj[$i]->postCategoryName;//explode("/",$postObj[$i]->postCategoryURL)[4]; ?></a></span><br><a href="#"><?php echo $postObj[$i]->postTitle; ?></a></h2>
            <p><?php echo $postObj[$i]->postExcerpt; ?></p>
        </div>
    <?php }?>
    </div>
</div>

<?php 
for($i = 0; $i < 3; $i++){
    // forming the URL from the keywords entered
    $link_url[$i] = 'http://research.wpcarey.asu.edu';
    $search_keyword = explode(",", $section_keywords[$i]);
    $link_url[$i] = $link_url[$i].'/?s='.$search_keyword[0];
    for($j=1;$j<count($search_keyword);$j++){
        if(strlen($search_keyword[$j])>=1){
            $link_url[$i] = $link_url[$i].'+'.$search_keyword[$j];
        }
    } 
    
    // updating the URL from the tags entered
     $search_tag = explode(",", $section_tags[$i]);
    // for($j=0;$j<count($search_tag);$j++){
    // //filtering text in search tags
        
        
    //     $search_tag[$j] = str_replace("/:*?<>|", "", $search_tag[$j]);
    // }
     
    if(count($search_tag)>0)
        $link_url[$i] = $link_url[$i].'&tag='.$search_tag[0];;
    for($j=1;$j<count($search_tag);$j++){
        if(strlen($search_tag[$j])>=1){
            $link_url[$i] = $link_url[$i].'+'.$search_tag[$j];
            $link_url[$i] = str_replace("'","", $link_url[$i]);
            $link_url[$i] = str_replace(" ","-", $link_url[$i]);     
        }
     } 
} 

?>


<div class="background-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 special">
                <div class="row">
                <?php for($i = 5, $j = 1; $i <= 16; $i+=4, $j++) { ?>
                    <div class="col-lg-4">
                        <?php echo "<h2 class='h4'><a href='".$link_url[$j-1]."'>".$section_titles[$j-1]."</a></h2>"?>
                        <?php echo "<a href='".$postObj[$i]->postLink."'><img src='".$postObj[$i]->postIMGUrl."' class='img-responsive'/> </a>  "; ?> 
                        <h3 class="h5"><a href="<?php echo $postObj[$i]->postLink; ?>"><?php echo $postObj[$i]->postTitle; ?></a></h3>
                        <table class="table table-condensed">
                           <tr><td><a href="<?php echo $postObj[$i+1]->postLink; ?>"><?php echo $postObj[$i+1]->postTitle; ?></a></td></tr>
                           <tr><td><a href="<?php echo $postObj[$i+2]->postLink; ?>"><?php echo $postObj[$i+2]->postTitle; ?></a></td></tr>
                           <tr><td><a href="<?php echo $postObj[$i+3]->postLink; ?>"><?php echo $postObj[$i+3]->postTitle; ?></a></td></tr>
                        </table>
                    </div>
                <?php }?>
                </div>
            </div>
            <aside class="col-lg-4">
                <div class="widget"><img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/04/spring-magazin-spring.jpg" class="img-responsive"></div>
            </aside>
        </div>
    </div>
</div>
<div class="container">
<div class="section-title text-center h2">W. P. Carey in the News</div>
    <div class="row">
        <div class="col-lg-4">
            <article class="nopadding">
                <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/08/Woma-stressed-glasses-IDEAS-380x215.jpg" class="img-responsive">
                <div class="panel-body">
                
                <h3 class="h4"><span class="source small"><a href="#">Harvard Business Review</a></span><br><a href="#">How anxiety affects CEO decision makint</a></h3>
                <p>In a Harvard Business Review article, Professor of Management Luis Gomez-Mejia and his research team reported on the...</p>
                </div>
            </article>
        </div>
        <div class="col-lg-4">
            <article class="nopadding">
                <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/05/Payday-IDEAS-380x215.jpg" class="img-responsive">
                <div class="panel-body">
                
                <h3 class="h4"><span class="source small"><a href="#">Bloomberg</a></span><br><a href="#">Wages are imperfect window into health of U.S. labor market</a></h3>
                <p>Up until recently the U.S. labor market has been viewed as on the uptick, especially after research conducted by the...</p>
                </div>
            </article>
        </div>
        <div class="col-lg-4">
            <article class="nopadding">
                <img src="http://research.wpcarey.asu.edu/wp-content/uploads/2016/08/People-job-applicants-IDEAS-380x215.jpg" class="img-responsive">
                <div class="panel-body">
                	
                	<h3 class="h4"><span class="source small"><a href="#">The Arizona Republic</a></span><br><a href="#">Arizona’s jobless rate climbs fourth month in a rot</a></h3>
                	<p>Arizona's unemployment rate rose in July for the fourth consecutive month, which dampens news that the state's</p>
                </div>
            </article>
        </div>
    </div>
</div>

<?php 

}?>