<?php 
function load_post(){
 global $wpdb; 
    $table_name = $wpdb->prefix . "selected_posts";
    $result = $wpdb->get_results("SELECT * FROM $table_name where id = 1 limit 1");

    //$returned_posts = new WP_Query( array))

    foreach($result as $row){
        $arrayOfPostIds = array( 
            $row->section1_post,
            $row->section2_post,
            $row->section3_post,
            $row->section4_post,
            $row->section5_post,
            $row->section6_1_post,
            $row->section6_2_post,
            $row->section6_3_post,
            $row->section6_4_post,
            $row->section7_1_post,
            $row->section7_2_post,
            $row->section7_3_post,
            $row->section7_4_post,
            $row->section8_1_post,
            $row->section8_2_post,
            $row->section8_3_post,
            $row->section8_4_post
        );
    }

    for($j = 0; $j<17; $j++){
        $arrayOfPosts[$j] = ($arrayOfPostIds[$j] < 0 ? "" : new WP_Query( array("p" => $arrayOfPostIds[$j] )));
    }

    $section6_title = $wpdb->get_var("SELECT section6_title FROM $table_name where id = 1 limit 1");
    $section7_title = $wpdb->get_var("SELECT section7_title FROM $table_name where id = 1 limit 1");
    $section8_title = $wpdb->get_var("SELECT section8_title FROM $table_name where id = 1 limit 1");

    $section6_keywords = $wpdb->get_var("SELECT section6_keywords FROM $table_name where id = 1 limit 1");
    $section7_keywords = $wpdb->get_var("SELECT section7_keywords FROM $table_name where id = 1 limit 1");
    $section8_keywords = $wpdb->get_var("SELECT section8_keywords FROM $table_name where id = 1 limit 1");

    $section6_tags = $wpdb->get_var("SELECT section6_tags FROM $table_name where id = 1 limit 1");
    $section7_tags = $wpdb->get_var("SELECT section7_tags FROM $table_name where id = 1 limit 1");
    $section8_tags = $wpdb->get_var("SELECT section8_tags FROM $table_name where id = 1 limit 1");

    $section_titles = array($section6_title,$section7_title,$section8_title);
    $section_keywords = array($section6_keywords,$section7_keywords,$section8_keywords);
    $section_tags = array($section6_tags,$section7_tags,$section8_tags);

    $sectionTitleObject = (object)array('section_titles' => $section_titles?$section_titles:"",
            'section_keywords' => $section_keywords?$section_keywords:"",
            'section_tags' => $section_tags?$section_tags:""
            );


    for($j = 0; $j<17;$j++){
        if($arrayOfPosts[$j]){
          $isFeatured = ($j===0)?'featured':'';
          //while ( $arrayOfPosts[$j]->have_posts() ) : 
          $arrayOfPosts[$j]->the_post();     
          $selectedPostId[$j] = $arrayOfPosts[$j]->query['p'];                      
          $postThumbnailId[$j] = get_post_thumbnail_id($selectedPostId[$j]);  
          $selectedPostFeaturedImageURL[$j] = wp_get_attachment_image_src($postThumbnailId[$j], $isFeatured)[0];                          
          //endwhile;
          $postCategory[$j] = get_the_category();

          if($postCategory[$j][0]){           
            $selectedPostCategoryURL[$j] = get_category_link($postCategory[$j][0]->term_id);   
            $selectedPostCategoryName[$j] = get_the_category($postCategory[$j][0]->term_id); 
          }
          $selectedPostTitle[$j] = get_the_title(); 
          $selectedPostExcerpt[$j] = get_the_excerpt(); 
          $selectedPostLink[$j] = get_the_permalink(false); 

          $postObj[$j] = (object)array('postTitle' => $selectedPostTitle[$j]?$selectedPostTitle[$j]:"",
            'postExcerpt' => $selectedPostExcerpt[$j]?$selectedPostExcerpt[$j]:"",
            'postLink' => $selectedPostLink[$j]?$selectedPostLink[$j]:"",
            'postIMGUrl' => $selectedPostFeaturedImageURL[$j]?$selectedPostFeaturedImageURL[$j]:"",
            'postCategoryURL' => $selectedPostCategoryURL[$j]?$selectedPostCategoryURL[$j]:"",
            'postCategoryName' => $selectedPostCategoryName[$j]?$selectedPostCategoryName[$j][0]->name:""
            );

        } else{
            $selectedPostTitle[$j] = "";
        }
    }
      return (array($postObj,$sectionTitleObject)); // Posts' details object

} ?>




