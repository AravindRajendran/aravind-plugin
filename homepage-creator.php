<?php
/*
Plugin Name: Homepage Creator
Plugin URI: 
Description: This plugin helps user populate the homepage with posts and publish on http://research.wpcarey.asu.edu/
Author: Aravind
Version: 1.1
Author URI: none
*/

//Filter to replace the template of the home page
add_filter( 'template_include', 'replace_home_page' );
function replace_home_page( $template ) {
  if ( is_front_page() ) {
    $postObjAll[0] = load_post();
    return plugin_dir_path( __FILE__ ) . 'new_template2.php';
  }
  return $template;
}

function homepage_creator() {
  wp_register_style('homepage_creator', plugins_url('homepage.css',__FILE__ ));
  wp_enqueue_style('homepage_creator');
}

add_action( 'wp_print_scripts','homepage_creator');
include ("load-post.php");
include ("render-post2.php");

function homepage_creator_plugin_js_enqueue(){
    wp_register_style('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
    wp_register_style('homepage-creator-css', plugin_dir_url(__FILE__) . 'homepage-creator.css');
    wp_register_script('bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js' , array('jquery'));
    wp_enqueue_script('bootstrap-js');
    wp_enqueue_style('bootstrap');
    wp_enqueue_style('homepage-creator-css');
    wp_register_script('homepage-creator-js', plugin_dir_url(__FILE__) . 'homepage-creator.js', array('jquery'));
    wp_enqueue_script('homepage-creator-js');
}

add_action('admin_enqueue_scripts', 'homepage_creator_plugin_js_enqueue');
add_action('admin_menu', 'homepage_creator_plugin_menu');


function homepage_creator_plugin_menu() {
    add_options_page( 'Homepage Creator Options', 'Homepage Creator', 'manage_options', 'homepagecreator',  'homepage_creator_plugin_options'  );
}


// Register hooks
add_action('admin_enqueue_scripts', 'add_script');
add_action('admin_enqueue_scripts', 'add_script_config');
//add_action('admin_head', 'add_script_config');

/**
 * Add script to admin page
 */
function add_script() {
    // Build in tag auto complete script
    // wp_enqueue_script( 'suggest' );
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-autocomplete');

}

/**
 * add script to admin page
 */
function add_script_config() {
?>

    <script type="text/javascript" >
    // Function to add auto suggest
    function setSuggest(id) {
        jQuery('#tags6').suggest("<?php echo get_bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php?action=ajax-tag-search&tax=post_tag", {multiple:true, multipleSep: ","}).attr("autocomplete","on");
    }
    </script>
<?php
}





// to create text field for the headings
function createTextField($text,$i,$keywords,$tags){
    if ($text){
        echo '<input name ="heading'.$i.'" type="text" value="'.$text.'"> </input>';
    } else{
        echo '<input name ="heading'.$i.'" type="text" value="empty"> </input>';
    }

    if ($keywords){
        echo '<h5>Keywords:</h5><input name ="keywords'.$i.'" type="text" value="'.$keywords.'"> </input>';
    } else{
        echo '<h5>Keywords:</h5><input name ="keywords'.$i.'" type="text" value="empty"> </input>';
    }
    if ($tags){
        echo '<h5>Tags:</h5><input id ="tags'.$i.'" name ="tags'.$i.'" type="text" autocomplete="on" onfocus="setSuggest(\'tags6\')"> </input>';
        echo '<div id="tagsdiv-post_tag"></div>';
    } else{
        echo '<h5>Tags:</h5><input id ="tags'.$i.'"name ="tags'.$i.'" type="text" value="empty" autocomplete="on"> </input>';


    }
    echo "<hr>";
}

// to create and populate drop down on page load value="'.$tags.'" 
function createDropDown($selected_post_id,$i){
    $featured_query = new WP_Query( array( 'posts_per_page' => 15 ) );
    $selected_post_flag = 1;

    echo '<select style="max-width:100%" name="selection'.$i.'">';

    while ($featured_query->have_posts()): $featured_query->the_post();
        if ($featured_query->post->ID == $selected_post_id || $selected_post_id == -1):
            $selected_post_flag=0;
        endif;
        echo '<option value='. $featured_query->post->ID.' ' . ($featured_query->post->ID == $selected_post_id ? ('selected' ): '' ) .'>' .get_the_title() .'</option>';
    endwhile;

    echo '<option value="-1" ' . (($selected_post_id == -1 || !$selected_post_id )? 'selected' : '' ) .'>--------Empty--------</option>';
    echo '<option value="-2"' . ( $selected_post_flag ? 'selected' : '' ) .'>--------Custom--------</option>';
    echo '</select>';

    if($selected_post_flag){
        echo '<input type="text" value="'.$selected_post_id.'" class="custom" name="selection'.$i.'" >';
    }

    wp_reset_postdata();
}

function homepage_creator_plugin_options() {
    if ( !current_user_can( 'manage_options' ) )  {
        wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
    }?>
    <div class="container-fluid">
        <div class="row">
            <h2>Welcome to Homepage Creator</h2>
            <form id="my-form" method="post" action="options-general.php?page=my-unique-identifier" class="col-md-3 ">
            <?php
                settings_fields( 'myoption-group' );
                do_settings_sections( 'myoption-group' );   
                global $wpdb;
                $table_name = $wpdb->prefix . "selected_posts";
                $result = $wpdb->get_results("SELECT * FROM $table_name where id = 1 limit 1");
            ?>
            <?php foreach($result as $row){ ?>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                   <div class="panel panel-default">
                       <div class="panel-heading" role="tab" id="headingOne">
                           <h4 class="panel-title">
                           <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne "    aria-expanded="true" aria-controls="collapseOne">
                              Top Section
                           </a>
                           </h4>
                       </div>
                       <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-     labelledby="  headingOne">
                           <div class="panel-body">
                               <?php createDropDown($row->section1_post,1); ?>
                           </div>
                       </div>
                   </div>
                   <div class="panel panel-default">
                       <div class="panel-heading" role="tab" id="headingTwo">
                         <h4 class="panel-title">
                           <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"   href ="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                             Middle Section
                           </a>
                         </h4>
                       </div>
                       <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="  headingTwo">
                           <div class="panel-body">
                               <?php createDropDown($row->section2_post,2); ?>
                               <?php createDropDown($row->section3_post,3); ?>
                               <?php createDropDown($row->section4_post,4); ?>
                               <?php createDropDown($row->section5_post,5); ?>
                           </div>
                       </div>
                   </div>
                   <div class="panel panel-default">
                       <div class="panel-heading" role="tab" id="headingThree">
                         <h4 class="panel-title">
                           <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"   href ="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                             Section 6
                           </a>
                         </h4>
                       </div>
                       <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby ="headingThree">
                            <div class="panel-body">
                                <h5>Title:</h5>
                                <?php createTextField($row->section6_title,6,$row->section6_keywords,stripslashes($row->section6_tags)); ?>
                                <h5>Featured Post:</h5>
                                <?php createDropDown($row->section6_1_post,6); ?>
                                <hr>
                                <?php createDropDown($row->section6_2_post,7); ?>
                                <?php createDropDown($row->section6_3_post,8); ?>
                                <?php createDropDown($row->section6_4_post,9); ?>
                            </div>
                       </div>
                   </div>
                   <div class="panel panel-default">
                       <div class="panel-heading" role="tab" id="headingFour">
                         <h4 class="panel-title">
                           <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"   href ="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                             Section 7
                           </a>
                         </h4>
                       </div>
                       <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby= "headingFour">
                           <div class="panel-body">
                                <h5>Title:</h5> 
                                <?php createTextField($row->section7_title,7,$row->section7_keywords,stripslashes($row->section7_tags)); ?>
                                <h5>Featured Post:</h5>
                                <?php createDropDown($row->section7_1_post,10); ?>
                                <hr>
                                <?php createDropDown($row->section7_2_post,11); ?>
                                <?php createDropDown($row->section7_3_post,12); ?>
                                <?php createDropDown($row->section7_4_post,13); ?>
                           </div>
                       </div>
                   </div>
                   <div class="panel panel-default">
                       <div class="panel-heading" role="tab" id="headingFive">
                         <h4 class="panel-title">
                           <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"   href ="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                             Section 8
                           </a>
                         </h4>
                       </div>
                       <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby= "headingFive">
                           <div class="panel-body">
                               <h5>Title:</h5> 
                               <?php createTextField($row->section8_title,8,$row->section8_keywords,stripslashes($row->section8_tags)); ?>
                               <h5>Featured Post:</h5>
                               <?php createDropDown($row->section8_1_post,14); ?>
                               <hr>
                               <?php createDropDown($row->section8_2_post,15); ?>
                               <?php createDropDown($row->section8_3_post,16); ?>
                               <?php createDropDown($row->section8_4_post,17); ?>
                           </div>
                       </div>
                    </div>
                </div>
                <?php submit_button(); ?>
            <?php } ?>
                <input type="hidden" value="generate_homepage" name="action">
            </form>
            <div id="result" class="col-md-9">
                <?php $postObjAll = load_post();
                echo render_post2($postObjAll); ?>
            </div>
        </div>
    </div>
    <?php 
}

function generate_homepage(){
    insert_record_into_selected_post();
    $postObjAll = load_post();
    echo render_post2($postObjAll);
}

add_action("wp_ajax_generate_homepage", "generate_homepage");
add_action("wp_ajax_nopriv_generate_homepage", "generate_homepage");

$initial_load_flag = 1;



function render_post($postObj){ 

          print_r($postObj[$j]->postTitle); echo"<br>";          
          print_r($postObj[$j]->postExcerpt); echo"<br>";
          print_r($postObj[$j]->postLink); echo"<br>";
          print_r($postObj[$j]->postIMGUrl); echo"<br>";                 
          print_r($postObj[$j]->postCategoryURL); echo"<br><br><br>";
          die();

  ?>
    <div class="row" >
        <?php $title = ($selectedPostTitle[0]? $selectedPostTitle[0] : 'Empty'); ?>
        <div class='col-lg-12'>
            <section class="<?php echo ($title === 'Empty'? 'empty' : ''); ?> ' >">
                <h4 class="title-1"><?php echo $title; ?></h4>
            </section>
        </div>
    </div>
    <div class="row">
    <?php for($i = 1; $i <= 4; $i++) { ?>
        <?php $title = ($selectedPostTitle[$i]? $selectedPostTitle[$i] : 'Empty'); ?>
        <div class='col-lg-3'>
            <section class="<?php echo ($title === 'Empty'? 'empty' : ''); ?> ' >">
                <h4 class="title-<?php echo ($i+1); ?>"><?php echo ($selectedPostTitle[$i]? $selectedPostTitle[$i] : '<span class="alert">' . $title . '</span>') ?></h4>
            </section>
        </div>
    <?php } ?>
    </div>
    <div class="row">
    <?php 
    $section_counter = 6;
    $section_titles = array("section6_title","section7_title","section8_title");
    $section_keywords = array("section6_keywords","section7_keywords","section8_keywords");
    $section_tags = array("section6_tags","section7_tags","section8_tags");
    ?>
    <?php for($i = 6; $i <= 14; $i+=4){?>
        <?php $title = ($selectedPostTitle[$i-1]? $selectedPostTitle[$i-1] : 'Empty'); ?>
        <div class="col-lg-4">
            <section class="bottom">

            <?php 
            //forming the URL from the keywords entered
            $link_url = 'http://research.wpcarey.asu.edu/?';
            $search_keyword = explode(" ", ${$section_keywords[$section_counter-6]});
            $link_url = $link_url.'s='.$search_keyword[0];

            for($j=1;$j<count($search_keyword);$j++){
              if(strlen($search_keyword)>=1){
                $link_url = $link_url.'+'.$search_keyword[$j];
              }
            } 

            //updating the URL from the tags entered
            $search_tag = explode(" ", ${$section_tags[$section_counter-6]});
            
            for($j=0;$j<count($search_tag);$j++){
              if(strlen($search_tag)>=1){
                $link_url = $link_url.'+'.$search_tag[$j];
              }
            } ?>
              
            <?php echo '<h4 class="title-' . ($section_counter) . '"> <a href="'.$link_url.'" target="_blank">' . ${$section_titles[$section_counter-6]} . '</h4>'; ?>
            <ul>
                <li class="featured <?php echo ($title === 'Empty'? 'empty' : ''); ?>"><h5><?php echo $title ?></h5></li>
                  <?php echo ($selectedPostTitle[$i]!=''? '<li class="'.$section_counter.'.1">'.$selectedPostTitle[$i].'</li>':' ')?> 
                  <?php echo ($selectedPostTitle[$i+1]!=''? '<li class="'.$section_counter.'.2">'.$selectedPostTitle[$i+1].'</li>':'')?> 
                  <?php echo ($selectedPostTitle[$i+2]!=''? '<li class="'.$section_counter.'.3">'.$selectedPostTitle[$i+2].'</   li>':'')?> 
            </ul>
            </section>
        </div>
        <?php $section_counter++;?>
    <?php } ?>    
    </div>
    <?php
}

function insert_record_into_selected_post(){
    global $wpdb;
    if($_POST["selection1"]){
        // select posts if from drop list or custom post text box
        $postIDArray = array(
            $_POST["selection1"],
            $_POST["selection2"],
            $_POST["selection3"],
            $_POST["selection4"],
            $_POST["selection5"],  
            $_POST["selection6"],
            $_POST["selection7"],
            $_POST["selection8"],
            $_POST["selection9"],
            $_POST["selection10"],
            $_POST["selection11"],
            $_POST["selection12"],
            $_POST["selection13"],
            $_POST["selection14"],
            $_POST["selection15"],
            $_POST["selection16"],
            $_POST["selection17"]
        );
        $postTitleArray = array(
            $_POST["heading6"],
            $_POST["heading7"],
            $_POST["heading8"]
        );

        $postKeywordArray = array(
            $_POST["keywords6"],
            $_POST["keywords7"],
            $_POST["keywords8"]
        );
        $postTagsArray = array(
            $_POST["tags6"],
            $_POST["tags7"],
            $_POST["tags8"]
        );
        

    }
    $table_name = $wpdb->prefix . "selected_posts";

    $wpdb->update(
        $table_name, 
        array(
            'section1_post'   => $postIDArray[0],
            'section2_post'   => $postIDArray[1],
            'section3_post'   => $postIDArray[2],
            'section4_post'   => $postIDArray[3],
            'section5_post'   => $postIDArray[4], 
            'section6_title'  => (String)$postTitleArray[0],
            'section6_1_post' => $postIDArray[5],
            'section6_2_post' => $postIDArray[6],
            'section6_3_post' => $postIDArray[7],
            'section6_4_post' => $postIDArray[8],
            'section7_title'  => (String)$postTitleArray[1],
            'section7_1_post' => $postIDArray[9],
            'section7_2_post' => $postIDArray[10],
            'section7_3_post' => $postIDArray[11],
            'section7_4_post' => $postIDArray[12],
            'section8_title'  => (String)$postTitleArray[2],
            'section8_1_post' => $postIDArray[13],
            'section8_2_post' => $postIDArray[14],
            'section8_3_post' => $postIDArray[15],
            'section8_4_post' =>$postIDArray[16],
            'section6_keywords' => (String)$postKeywordArray[0],
            'section7_keywords' => (String)$postKeywordArray[1],
            'section8_keywords' => (String)$postKeywordArray[2],
            'section6_tags' => (String)$postTagsArray[0],
            'section7_tags' => (String)$postTagsArray[1],
            'section8_tags' => (String)$postTagsArray[2]
        ),
        array( 'id' => 1 )
    );
}

function homepage_install(){
    global $wpdb;
    
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . "selected_posts"; 

    /*Creating new table */
    $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL,
        section1_post int(5) NOT NULL,
        section2_post int(5) NOT NULL,
        section3_post int(5) NOT NULL,
        section4_post int(5) NOT NULL,
        section5_post int(5) NOT NULL,
        section6_title varchar(50),
        section6_1_post int(5) NOT NULL,
        section6_2_post int(5) NOT NULL,
        section6_3_post int(5) NOT NULL,
        section6_4_post int(5) NOT NULL,
        section7_title varchar(50),
        section7_1_post int(5) NOT NULL,
        section7_2_post int(5) NOT NULL,
        section7_3_post int(5) NOT NULL,
        section7_4_post int(5) NOT NULL,
        section8_title varchar(50),
        section8_1_post int(5) NOT NULL,
        section8_2_post int(5) NOT NULL,
        section8_3_post int(5) NOT NULL,
        section8_4_post int(5) NOT NULL,
        section6_keywords varchar(50),
        section7_keywords varchar(50),
        section8_keywords varchar(50),
        section6_tags varchar(50),
        section7_tags varchar(50),
        section8_tags varchar(50)
    ) $charset_collate;";
    
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta($sql);
}
function homepage_install_data() {
    global $wpdb;
    $table_name = $wpdb->prefix . "selected_posts";

    $wpdb->insert(
        $table_name,
        array(
            'id'              => 1,
            'section1_post'   => -1,
            'section2_post'   => -1,
            'section3_post'   => -1,
            'section4_post'   => -1,
            'section5_post'   => -1, 
            'section6_title'  => "",
            'section6_1_post' => -1,
            'section6_2_post' => -1,
            'section6_3_post' => -1,
            'section6_4_post' => -1,
            'section7_title'  => "",
            'section7_1_post' => -1,
            'section7_2_post' => -1,
            'section7_3_post' => -1,
            'section7_4_post' => -1,
            'section8_title'  => "",
            'section8_1_post' => -1,
            'section8_2_post' => -1,
            'section8_3_post' => -1,
            'section8_4_post' => -1,
            'section6_keywords' => "",
            'section7_keywords' => "",
            'section8_keywords' => "",
            'section6_tags'   => "",
            'section7_tags'   => "",
            'section8_tags'   => ""
        )
    );
}

register_activation_hook( __FILE__, 'homepage_install' );
register_activation_hook( __FILE__, 'homepage_install_data' );

