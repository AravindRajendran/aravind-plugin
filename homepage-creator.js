 (function($) {

     $(document).ready(function() {
         function myPlugin() {
             $.ajax({
                 url: ajaxurl,
                 type: 'POST',
                 data: $('#my-form').serialize(),
                 success: function(data) {
                     $('#result').html(data);

                 },
                 error: function(errorThrown) {
                     console.log(errorThrown);
                 }
             });
         }
    $('#my-form').submit(function(event) {
             event.preventDefault();
             myPlugin();
    });

    $('#my-form select').change(function(){
        
    if($(this).attr('value') === '-2'){
        $(this).after('<input type="text" value="" placeholder="Enter Post Id" class="custom" name="' + $(this).attr('name') + '" >');
    } else{
            $next = $(this).next();
            if($next.hasClass('custom')){
             $next.remove();
            }
            }
    });
    });
 }(jQuery));
